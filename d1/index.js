/*
   Functions are lines/blocks of codes that tell our devices/application to perform certain task/s when called/invoked.
   Functions are mostly created to create complicated tasks to run several lines of codes in succession.
   They are also used to prevent repeatition of typing lines/blocks of codes that perform the same task.
*/
/*
   Miniactivity
      create a function
         create a let variable nickname, the value should come from a prompt();
         log in the console the variable

      invoke the function
*/

/*function printInput(){
   let nickname = prompt( "Enter your nickname" );
   console.log( nickname );
};

printInput();*/


// However, for some use cases, this may not be ideal. For other cases, functions can also process data directly passed into it instead of relying only on global variables such as prompt();

// Consider this function:
// we can directly pass data into thhe function. as for the function below, the data is referred to as the "name" within the printName function. this "name" is what we called as the parameter.
/*
   PARAMETER
      -it acts as a named variable/container that exists only inside of a function;
      -it is used to store information that is provided to a function when it is called/invoked;
*/
function printName(name){
   console.log( "Hello " + name );
};
// ARGUMENTS
// "Joana", the information provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
printName( "Joana" );
// These two are both arguments since both of them are supplied as information that will be used to print out the full message of function. when the printName() function is called, "John" and "Jane" are stored in the paramater "name" then the function uses them to  print the message
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

/*
   Miniactivity
      create 2 functions
         1st function should have 2 parameters (separated with a comma)
            log in the console the message "the numbers passed as arguments are:"
            log in the console the first parameter (number) 
            log in the console the second parameter (number) 
         invoke the function
         
         2nd function should have 3 parameters (make use of comma to separate the parameters)
            log in the console "My friends are parameter1, parameter2, parameter3"
         invoke the function

         send the output in the batch google chat
*/
// multiple parameters are separated by comma
function printNumbers(num1, num2){
   console.log( "The numbers passed as arguments are: " );
   console.log( num1 );
   console.log( num2 );
}
// argument that will be stored in multiple parameters should also be separated by comma
printNumbers(5, 2);


function printFriends(friend1, friend2, friend3){
   console.log( "My friends are " + friend1 + " " + friend2 + " " + friend3 + "." );
}
printFriends( "Martin", "Kevin", "Jerome" );

// improving our codes for the last activity with the use of parameters
function checkDivisibilityBy8(num){
   let remainder = num%8;
   console.log("The remainder of " + num + " is " + remainder);
   let isDivisibleBy8 = remainder === 0;
   console.log("Is " + num + " divisible by 8?");
   console.log(isDivisibleBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
